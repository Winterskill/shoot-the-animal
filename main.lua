--[[
SHOOT THE ANIMAL
v1.0
]]

--[[
TODO:
beta 0.2: 
 - programmer le mode 2 joueurs
 - dans fichier data\2playerspanda.lua ligne 34
 - faire en sorte qu'on puisse rejoindre l'écran principal depuis le jeu
 - enlever les musiques superposées quand on finit un mode 1 joueur et qu'on passe au mode 2 joueurs
 - faire un système de hight score
 - faire une options "crédits" dans le menu principal
 - mentionner Kevin McLeod dans les crédits
 - faire un thème communiste

auteur : winterskill
]]

function love.load()
    sTitle = "Shoot the animal"
    love.window.setTitle(sTitle)
    Object = require "System/classic"
    tick = require "System/tick"
    
    require "Data/Player"
    require "Data/Enemy"
    require "Data/Bullet"
    require "Data/2PlayersBullet1"
    require "Data/2PlayersBullet2"
    require "Data/2PlayersPanda"
    require "Data/2PlayersSerpent"
    
    require "System/color"
    
    player = Player()
    enemy = Enemy()
    player1 = Panda()
    player2 = Serpent()
    bullets = {}
    score = 0
    win = false
    victoryMusicPlaying = false
    version = "Beta 0.2 Build xx/xx/xxxx"
	scoreColor = "green"
    
    Noise = {
        touche = love.audio.newSource("Audio/Sounds/touche.ogg", "static"),
        gameover = love.audio.newSource("Audio/Sounds/gameover.ogg", "static"),
        fire = love.audio.newSource("Audio/Sounds/attack.ogg", "static")
    }
    
    usedCheat = false
    
    
    -- easter egg
    alreadyBouhed = false
    winWithTrucABruno = false
    
    settings = {
        playMusic = true
    }
    
    bgm = love.audio.newSource("Audio/Musics/Laser Groove.mp3")
    bgm:setLooping(true)
    
    scene = {
        menu = {
            titleMenu = {
                id = 1,
                
                vars = {
                    cursor_position = 1,
                    max_cursor_position = 4,
                    texts = {
                        "1 Joueur",
                        "2 Joueurs",
                        "Quitter",
                        
                        "Settings"
                    },
                    texts_color = {
                        colorcss.green,
                        colorcss.blue,
                        colorcss.red,
                        colorcss.purple
                    },
                    credit = "HangusCorporation <http://blenden-mark.alwaysdata.net> | Winterskill | Version : " .. version,
                    bgm = love.audio.newSource("Audio/Musics/Digital Lemonade.mp3")
                }
            },
            settingsMenu = {
                id = 5,
                
                vars = {
                    cursor_position = 1,
                    max_cursor_position = 2,
                    texts = {
                        "Musique",
                        "Ecran-Titre"
                    },
                    texts_color = {
                        {colorcss.green, colorcss.red},
                        {colorcss.blue, colorcss.blue}
                    }
                }
            }
        },
        map = {
            onePlayer = {
                id = 2,
                
                vars = {}
            },
            twoPlayers = {
                id = 3,
                
                vars = {
                    gameWon = false,
                    winner = "",
                    player1bullets = {}, -- panda
                    player2bullets = {} -- serpent
                }
            },
            onePlayerSplash = {
                -- permet de ne pas utiliser la sulfateuse quand on appuie sur entrée et que ça nous téléporte au
                -- mode 1 joueur.
                -- De plus, donne les règles du mode de jeu. Pour continuer, appuyer sur `espace`.
                id = 4,
                
                vars = {
                    text = {colorsvg.White, [[Mode : 1 Joueur

Dans le mode 1 joueur, vous incarnez le Panda, et devez arriver à un score de 255 points pour gagner.
En appuyant sur espace, vous tirez. Vous gagnez 1 point chaque fois qu'un de vos tirs fait mouche sur le serpent.
Mais attention! Plus vous touchez le serpent, plus celui-ci sera rapide.

]], colorsvg.LightGreen, [[Pour continuer, appuyez sur <espace>]]}
                }
            },
            twoPlayersSplash = {
                id = 6,
                
                vars = {
                    text = {colorsvg.White, [[Mode : 2 Joueurs

Dans le mode 2 joueurs, l'un des 2 joueurs incarne le Panda, et l'autre le Serpent. Au début, le score est de 127.

Le but du Panda est d'arriver à un score de 255. Chaque fois qu'il touche le Serpent, le score augmente de 1.

Le but du Serpent est d'arriver à un score de 0. Chaque fois qu'il touche le Panda, le score baisse de 1.

Dans ce mode de jeu, tirer hors de l'écran ne fait pas perdre le jeu, mais fait perdre 2 points si c'est le Panda qui tire,
et en fait gagner 2 si c'est le Serpent qui tire.

]],colorsvg.FireBrick, [[Dans ce mode de jeu, la triche est désactivée.

Contrôles :
Joueur 1 : bouger : flèches droites et gauches
Joueur 1 : tirer : entrée
Joueur 2 : bouger : a et z
Joueur 2 : tirer : espace
]], colorsvg.LightGreen, [[Pour continuer, appuyez sur <espace>]]}
                }
            }
        }
    }
    
    scene.actual = scene.menu.titleMenu
    
    function reset()
        player = Player()
        enemy = Enemy()
        player1 = Panda()
        player2 = Serpent()
        bullets = {}
        score = 0
        win = false
        victoryMusicPlaying = false
        usedCheat = false
        winWithTrucABruno = false
        love.window.setTitle(sTitle)
    end
end


function love.update(delta_time)
	if cccpmode then
		love.window.setTitle(sTitle .. " [Mode CCCP]")
	end
	
    if scene.actual.id == scene.menu.titleMenu.id then
        -- title menu
        reset()
        if settings.playMusic then
            bgm:stop()
            scene.actual.vars.bgm:play()
        end
    elseif scene.actual.id == scene.map.onePlayer.id then
        -- mode 1 joueur
        if not win then
            -- si on a pas gagné
            player:update(delta_time)
            enemy:update(delta_time)
            for k,v in ipairs(bullets) do
                -- pour chaque balle
                v:update(delta_time)
            end
        else
            bgm:stop()
        end
        
        if settings.playMusic then
            bgm:play()
        end
    elseif scene.actual.id == scene.map.twoPlayers.id then
        -- mode 2 joueurs
        if not scene.actual.vars.gameWon then
            -- si personne n'a gagné
            player1:update(delta_time)
            player2:update(delta_time)
            
			if not scene.actual.vars.player1bullets == nil then
				for k,v in ipairs(scene.actual.vars.player1bullets) do
					v:update(delta_time)
				end
			end
			if not scene.actual.vars.player2bullets == nil then
				for k,v in ipairs(scene.actual.vars.player2bullets) do
					v:update(delta_time)
				end
			end
        else
            bgm:stop()
        end
        
        if settings.playMusic then
            bgm:play()
        end
    elseif scene.actual.id == scene.map.onePlayerSplash.id then
        -- écran de passage au mode 1 joueur
	elseif scene.actual.id == scene.map.twoPlayersSplash.id then
		-- écran de passage au mode 2 joueurs
    end
end


function love.draw()
    if scene.actual.id == scene.menu.titleMenu.id then
        -- title menu
        win = false -- désactivation de la victoire, pour pouvoir recommencer
        
        love.graphics.rectangle("line", 5, 5, love.graphics.getWidth() - 10, love.graphics.getHeight() - 10)
        love.graphics.rectangle("line", 10, 10, love.graphics.getWidth() - 20, love.graphics.getHeight() - 20)
        love.graphics.print({colorsvg.Olive, scene.actual.vars.credit}, 15, love.graphics.getHeight() - 25)
        
        -- placeholder
        love.graphics.rectangle("line", 20, 50, love.graphics.getWidth() - 50, 100)
        if cccpmode then
			love.graphics.print({{255,0,0}, "Shoot the American"}, 20, 50, 0, 7, 7)
		else
			love.graphics.print("Shoot the Animal", 20, 50, 0, 7, 7)
		end
        love.graphics.print("Placeholder", love.graphics.getWidth() / 2, 135)
        -- /placeholder
        
        -- menu
        if scene.actual.vars.cursor_position == 1 then
            love.graphics.setColor(scene.actual.vars.texts_color[1])
            love.graphics.rectangle("fill", love.graphics.getWidth() / 2 - 100, 200, 200, 50)
            love.graphics.setColor(colorcss.white)
        elseif scene.actual.vars.cursor_position == 2 then
            love.graphics.setColor(scene.actual.vars.texts_color[2])
            love.graphics.rectangle("fill", love.graphics.getWidth() / 2 - 100, 250, 200, 50)
            love.graphics.setColor(colorcss.white)
        elseif scene.actual.vars.cursor_position == 3 then
            love.graphics.setColor(scene.actual.vars.texts_color[3])
            love.graphics.rectangle("fill", love.graphics.getWidth() / 2 - 100, 300, 200, 50)
            love.graphics.setColor(colorcss.white)
        elseif scene.actual.vars.cursor_position == 4 then
            love.graphics.setColor(scene.actual.vars.texts_color[4])
            love.graphics.rectangle("fill", love.graphics.getWidth() / 2 - 100, 350, 200, 50)
            love.graphics.setColor(colorcss.white)
        end
        
        love.graphics.rectangle("line", love.graphics.getWidth() / 2 - 100, 200, 200, 50)
        love.graphics.rectangle("line", love.graphics.getWidth() / 2 - 100, 250, 200, 50)
        love.graphics.rectangle("line", love.graphics.getWidth() / 2 - 100, 300, 200, 50)
        love.graphics.rectangle("line", love.graphics.getWidth() / 2 - 100, 350, 200, 50)
        
        love.graphics.print(scene.actual.vars.texts[1], love.graphics.getWidth() / 2 - 100 + 15, 200, 0, 3)
        love.graphics.print(scene.actual.vars.texts[2], love.graphics.getWidth() / 2 - 100 + 15, 250, 0, 3)
        love.graphics.print(scene.actual.vars.texts[3], love.graphics.getWidth() / 2 - 100 + 15, 300, 0, 3)
        love.graphics.print(scene.actual.vars.texts[4], love.graphics.getWidth() / 2 - 100 + 15, 350, 0, 3)
        -- /menu
    elseif scene.actual.id == scene.menu.settingsMenu.id then
        -- menu paramètres
        love.graphics.rectangle("line", 5, 5, love.graphics.getWidth() - 10, love.graphics.getHeight() - 10)
        love.graphics.rectangle("line", 10, 10, love.graphics.getWidth() - 20, love.graphics.getHeight() - 20)
        
        -- menu
        local clr = scene.actual.vars.texts_color
        
        if scene.actual.vars.cursor_position == 1 then
            if settings.playMusic then
                love.graphics.setColor(clr[1][1])
            else
                love.graphics.setColor(clr[1][2])
            end
            love.graphics.rectangle("fill", love.graphics.getWidth() / 2 - 100, 200, 200, 50)
            love.graphics.setColor(colorcss.white)
        elseif scene.actual.vars.cursor_position == 2 then
            love.graphics.setColor(clr[2][1])
            love.graphics.rectangle("fill", love.graphics.getWidth() / 2 - 100, 250, 200, 50)
            love.graphics.setColor(colorcss.white)
        end
        
        love.graphics.rectangle("line", love.graphics.getWidth() / 2 - 100, 200, 200, 50)
        love.graphics.rectangle("line", love.graphics.getWidth() / 2 - 100, 250, 200, 50)
        
        love.graphics.print(scene.actual.vars.texts[1], love.graphics.getWidth() / 2 - 100 + 15, 200, 0, 3)
        love.graphics.print(scene.actual.vars.texts[2], love.graphics.getWidth() / 2 - 100 + 15, 260, 0, 2)
        -- /menu
    elseif scene.actual.id == scene.map.onePlayer.id then
        -- mode 1 joueur
        if not win then
            -- si on a pas encore gagné
            player:draw()
            enemy:draw()
            
            for k,v in ipairs(bullets) do
                -- pour chaque balle
                v:draw()
                v:checkCollision(enemy)
                
                if v.dead then
                    table.remove(bullets, k)
                    score = score + 1
                    love.window.setTitle(sTitle .. " - " .. tostring(score))
                    love.audio.play(Noise.touche)
                end
            end
            
            if enemy:checkCollision(player) then
                -- easter egg
                win = true
                winWithTrucABruno = true
            end
            
            -- affichage du score, en couleur
			if scoreColor == "green" then
				love.graphics.print({
					{-score, 255, -score},
					tostring(score)
					}, 10, 10, 0, score * (1 / 5), score * (1 / 5), (tostring(score):len() / 2), (tostring(score):len() / 2))
			elseif scoreColor == "red" then
				love.graphics.print({
					{255, -score, -score},
					tostring(score)
					}, 10, 10, 0, score * (1 / 5), score * (1 / 5), (tostring(score):len() / 2), (tostring(score):len() / 2))
			end
            -- affichage de la vitesse
            love.graphics.print({
                {255,0,0},
                (math.abs(tostring(enemy.speed)) / 5) / 10
            }, love.graphics.getWidth() - 50, 10)
            
            if score >= 255 then
                -- on a gagné!
                win = true
            end
            
            if love.keyboard.isDown("return") then
                player:keypressed("space")
            end
        else
            -- si on a gagné
            player = nil
            enemy = nil
            bullets = nil
            
            -- on joue la musique de victoire
            if not victoryMusicPlaying then
                victoryMusicPlaying = true
                if not cccpmode then
					victoryMusic = love.audio.newSource("Audio/Musics/Digital Lemonade.mp3")
				else
					victoryMusic = love.audio.newSource("Audio/Musics/Farewell of Slavianka.mp3")
				end
                victoryMusic:setLooping(true);
                victoryMusic:play();
            end
            
            if not winWithTrucABruno then
                love.graphics.rectangle("line", 5, 5, love.graphics.getWidth() - 10, love.graphics.getHeight() - 10)
                love.graphics.rectangle("line", 10, 10, love.graphics.getWidth() - 20, love.graphics.getHeight() - 20)
                love.graphics.print("V I C T O R Y", 375, 275)
                
                if not usedCheat then
                    -- si on a gagnés sans tricher
                    love.graphics.print({{000,255,000},"Et sans tricher!"}, 375, 300)
                end
            else
                -- si on a gagné avec le truc à bruno
                -- truc à bruno : réduire la hauteur de la fenêtre pour déclancher une collision entre le serpent et le panda
                love.graphics.rectangle("line", 5, 5, love.graphics.getWidth() - 10, love.graphics.getHeight() - 10)
                love.graphics.rectangle("line", 10, 10, love.graphics.getWidth() - 20, love.graphics.getHeight() - 20)
                
                love.graphics.print({colorcss.green, "GG! T'as trouvé l'easter egg!"}, 350, 275)
                love.graphics.print({colorcss.red, "Mais ça ne t'apportera R I E N!!!"}, 330, 300)
                love.graphics.print({colorcss.blue, "(enfin si! le mode 2 joueurs) (quand il sera programmé)"}, 300, 325)
                
                love.graphics.print({colorsvg.Olive, "(C) Game won with TrucABruno(TM), 2017"}, 15, love.graphics.getHeight() - 25)
            end
        end
    elseif scene.actual.id == scene.map.twoPlayers.id then
        -- mode 2 joueurs
        
        if not scene.actual.vars.gameWon then
            -- si personne n'a gagné
            player1:draw()
            player2:draw()
            
            for i,v in ipairs(scene.actual.vars.player1bullets) do
                v:draw()
                v:checkCollision(player2)
                
                if v.dead then
                    table.remove(scene.actual.vars.player1bullets, i)
                    score = score + 1
                    love.window.setTitle(sTitle .. " - " .. tostring(score))
                    love.audio.play(Noise.touche)
                end
            end
            
            for i,v in ipairs(scene.actual.vars.player2bullets) do
                v:draw()
                v:checkCollision(player1)
                
                if v.dead then
                    table.remove(scene.actual.vars.player2bullets, i)
                    score = score - 1
                    love.window.setTitle(sTitle .. " - " .. tostring(score))
                    love.audio.play(Noise.touche)
                end
            end
            
            -- affichage du score
            love.graphics.print({
                {-tostring(score), 255, -tostring(score)},
                tostring(score)
            }, 10, 10, 0, score * (1 / 5), score * (1 / 5), (tostring(score):len() / 2), (tostring(score):len() / 2))
            
            if score >= 255 then
                scene.actual.vars.winner = "Panda"
                scene.actual.vars.gameWon = true
            end
            
            if score <= 0 then
                scene.actual.vars.winner = "Serpent"
                scene.actual.vars.gameWon = true
            end
        else
            -- si il y a un gagnant
            player1 = nil
            player2 = nil
            scene.actual.vars.player1bullets = {}
            scene.actual.vars.player2bullets = {}
            
            if not victoryMusicPlaying then
                victoryMusicPlaying = true
                victoryMusic = love.audio.newSource("Audio/Musics/Digital Lemonade.mp3")
                victoryMusic:setLooping(true);
                victoryMusic:play();
            end
            
            love.graphics.rectangle("line", 5, 5, love.graphics.getWidth() - 10, love.graphics.getHeight() - 10)
            love.graphics.rectangle("line", 10, 10, love.graphics.getWidth() - 20, love.graphics.getHeight() - 20)
            love.graphics.print("V I C T O R Y", 375, 275)
        end
    elseif scene.actual.id == scene.map.onePlayerSplash.id then
        -- écran de passage au mode 1 joueur
        love.graphics.rectangle("line", 5, 5, love.graphics.getWidth() - 10, love.graphics.getHeight() - 10)
        love.graphics.rectangle("line", 10, 10, love.graphics.getWidth() - 20, love.graphics.getHeight() - 20)
        
        love.graphics.print(scene.actual.vars.text, 20, 20)
    elseif scene.actual.id == scene.map.twoPlayersSplash.id then
        -- écran de passage au mode 2 joueurs
        love.graphics.rectangle("line", 5, 5, love.graphics.getWidth() - 10, love.graphics.getHeight() - 10)
        love.graphics.rectangle("line", 10, 10, love.graphics.getWidth() - 20, love.graphics.getHeight() - 20)
        
        love.graphics.print(scene.actual.vars.text, 20, 20)
    end
end


function love.keypressed(key)
    if scene.actual.id == scene.menu.titleMenu.id then
        -- title menu
        
        if key == "up" then
            if scene.actual.vars.cursor_position > 1 then
                scene.actual.vars.cursor_position = scene.actual.vars.cursor_position - 1
            else
                scene.actual.vars.cursor_position = scene.actual.vars.max_cursor_position
            end
        elseif key == "down" then
            if scene.actual.vars.cursor_position < scene.actual.vars.max_cursor_position then
                scene.actual.vars.cursor_position = scene.actual.vars.cursor_position + 1
            else
                scene.actual.vars.cursor_position = 1
            end
        end
        
        if key == "return" then
            scene.actual.vars.bgm:stop()
            if scene.actual.vars.cursor_position == 1 then
                -- mode 1 joueur
                scene.actual = scene.map.onePlayerSplash
            elseif scene.actual.vars.cursor_position == 2 then
                -- mode 2 joueurs
                --love.window.showMessageBox("", "Pas encore programmé!", {":("})
                --love.window.showMessageBox("", "Mais ça ne devrait pas tarder...", {"Bon, ok..."})
                scene.actual = scene.map.twoPlayersSplash
            elseif scene.actual.vars.cursor_position == 3 then
                -- quitter
                love.event.quit()
            elseif scene.actual.vars.cursor_position == 4 then
                scene.actual = scene.menu.settingsMenu
            end
        end
    elseif scene.actual.id == scene.menu.settingsMenu.id then
        -- le menu des paramètres
        if key == "up" then
            if scene.actual.vars.cursor_position > 1 then
                scene.actual.vars.cursor_position = scene.actual.vars.cursor_position - 1
            else
                scene.actual.vars.cursor_position = scene.actual.vars.max_cursor_position
            end
        elseif key == "down" then
            if scene.actual.vars.cursor_position < scene.actual.vars.max_cursor_position then
                scene.actual.vars.cursor_position = scene.actual.vars.cursor_position + 1
            else
                scene.actual.vars.cursor_position = 1
            end
        end
        
        if key == "return" then
            if scene.actual.vars.cursor_position == 1 then
                -- musique on/off
                settings.playMusic = not settings.playMusic
            elseif scene.actual.vars.cursor_position == 2 then
                scene.actual = scene.menu.titleMenu
            end
        end
    elseif scene.actual.id == scene.map.onePlayer.id then
        -- mode 1 joueur
        if not win then
            -- si on a pas gagné
            player:keypressed(key)
            if key == "escape" then
                scene.actual = scene.menu.titleMenu
            end
            
            if key == "kp+" or key == "=" then
                score = score + 10
                usedCheat = true
            end
            if key == "kp-" or key == "-" then
                usedCheat = true
                if score - 10 < 0 then
                    score = 0
                else
                    score = score - 10
                end
            end
            
            if key == "up" then
                usedCheat = true
                score = score + 1
            end
            if key == "down" then
                usedCheat = true
                if score - 1 < 0 then
                    score = 0
                else
                    score = score - 1
                end
            end
        else
            -- si on a gagné
			bgm:stop()
            scene.actual = scene.menu.titleMenu
        end
    elseif scene.actual.id == scene.map.twoPlayers.id then
        if not scene.actual.vars.gameWon then
            -- si il n'y a pas de gagnant
            player1:keypressed(key)
            player2:keypressed(key)
        end
    elseif scene.actual.id == scene.map.onePlayerSplash.id then
        -- écran de passage au mode 1 joueur
        if key == "space" then
            scene.actual = scene.map.onePlayer
        end
    elseif scene.actual.id == scene.map.twoPlayersSplash.id then
        -- écran de passage au mode 2 joueurs
        if key == "space" then
            scene.actual = scene.map.twoPlayers
            score = 127
        end
    end
end


function love.keyreleased(key)
    if scene.actual.id == scene.menu.titleMenu.id then
        -- title menu
    elseif scene.actual.id == scene.map.onePlayer.id then
        -- mode 1 joueur
    elseif scene.actual.id == scene.map.twoPlayers.id then
        -- mode 2 joueurs
    elseif scene.actual.id == scene.map.onePlayerSplash.id then
        -- écran de passage au mode 1 joueur
    end
    
    -- global
	if key == "f12" then
		--local is_fullscreen = love.window.getFullscreen()
		love.window.setFullscreen(not love.window.getFullscreen())
	end
    
    if key == "f11" then
        if alreadyBouhed == false then
            alreadyBouhed = true
            if love.window.showMessageBox("", "bouh!", {"Aaah!"}) == 1 then
                local a = love.window.showMessageBox("", "Héhéhé", {"...", ""})
                if a == 1 then
                elseif a == 2 then
                    love.event.quit()
                end
            end
        else
            love.window.showMessageBox("", "Déjà fait!", {"Bon, OK"})
        end
	elseif key == "f10" then
		local a = love.window.showMessageBox("Crédits", "Programmation : Winterskill\nMusique : Kevin MacLeod", {"Bien reçu!", "Dacodac", "aplha du centaure", "CCCP", "lamasticot"})
		
		if a == 1 then
			love.window.showMessageBox("", "")
		elseif a == 4 then
			cccpmode = true
			bgm = love.audio.newSource("Audio/Musics/Katyusha.mp3")
			scene.actual.vars.bgm:stop()
			scene.menu.titleMenu.vars.bgm = love.audio.newSource("Audio/Musics/Farewell of Slavianka.mp3")
			scoreColor = "red"
		end
    end
end