Serpent = Object:extend()

function Serpent:new()
  self.image = love.graphics.newImage("Graphics/Characters/snake.png")
  self.speed = 300
  self.width = self.image:getWidth()
  self.height = self.image:getHeight()
  self.x = 350
  self.y = love.graphics.getHeight() - self.height - 20
end


function Serpent:update(delta_time)
  if love.keyboard.isDown("z") then
    self.x = self.x + self.speed * delta_time
  elseif love.keyboard.isDown("a") then
    self.x = self.x - self.speed * delta_time
  end
  
  if self.x < 0 then
    self.x = 0
  elseif self.x + self.width > love.graphics.getWidth() then
    self.x = love.graphics.getWidth() - self.width
  end
end


function Serpent:draw()
  love.graphics.draw(self.image, self.x, self.y)
end


function Serpent:keypressed(key)
  if key == "space" then
    table.insert(scene.actual.vars.player2bullets, Bullet2(self.x + (self.width / 2), self.y))
    love.audio.play(Noise.fire)
  end
end