Bullet1 = Object:extend()

function Bullet1:new(x, y)
  self.image = love.graphics.newImage("Graphics/Characters/bullet.png")
  self.x = x
  self.y = y
  self.speed = 700
  self.width = self.image:getWidth()
  self.height = self.image:getHeight()
  self.dead = false
end


function Bullet1:update(delta_time)
  self.y = self.y + self.speed * delta_time
  
  if self.y > love.graphics.getHeight() then
    love.audio.play(Noise.gameover)
    score = score - 2
  end
end


function Bullet1:draw()
  love.graphics.draw(self.image, self.x, self.y)
end


function Bullet1:checkCollision(obj)
  if self.x + self.width > obj.x and self.x < obj.x + obj.width and self.y + self.height > obj.y and self.y < obj.y + obj.height then
    self.dead = true
    
    if obj.speed > 0 then
      obj.speed = obj.speed + 50
    else
      obj.speed = obj.speed - 50
    end
  end
end