Enemy = Object:extend()

function Enemy:new()
  self.image = love.graphics.newImage("Graphics/Characters/snake.png")
  self.x = 325
  self.y = 450
  self.speed = 100
  self.width = self.image:getWidth()
  self.height = self.image:getHeight()
end


function Enemy:update(delta_time)
  self.x = self.x + self.speed * delta_time
  
  if self.x < 0 then
    self.x = 0
    self.speed = -self.speed
  elseif self.x + self.width > love.graphics.getWidth() then
    self.x = love.graphics.getWidth() - self.width
    self.speed = -self.speed
  end
  
  -- easter egg
  if self.y + self.height > love.graphics.getHeight() then
    self.y = love.graphics.getHeight() - self.height
  end
end


function Enemy:draw()
  love.graphics.draw(self.image, self.x, self.y)
end


function Enemy:checkCollision(obj)
  if self.x + self.width > obj.x and self.x < obj.x + obj.width and self.y + self.height > obj.y and self.y < obj.y + obj.height then
    return true
  else
	return false
  end
end