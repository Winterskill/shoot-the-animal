Player = Object:extend()

function Player:new()
  self.image = love.graphics.newImage("Graphics/Characters/panda.png")
  self.speed = 300
  self.x = 350
  self.y = 20
  self.width = self.image:getWidth()
  self.height = self.image:getHeight()
end


function Player:update(delta_time)
  if love.keyboard.isDown("right") then
    self.x = self.x + self.speed * delta_time
  elseif love.keyboard.isDown("left") then
    self.x = self.x - self.speed * delta_time
  end
  
  if self.x < 0 then
    self.x = 0
  elseif self.x + self.width > love.graphics.getWidth() then
    self.x = love.graphics.getWidth() - self.width
  end
end


function Player:draw()
  love.graphics.draw(self.image, self.x, self.y)
end


function Player:keypressed(key)
  if key == "space" then
    table.insert(bullets, Bullet(self.x + (self.width / 2), self.y))
    love.audio.play(Noise.fire)
  end
end